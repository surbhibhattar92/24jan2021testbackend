const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CatSchema = new Schema({
  text: String,
  img: String,
  createdOn: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("cats", CatSchema);
