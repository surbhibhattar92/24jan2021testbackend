const Cat = require("../models/Cat");

//List all available cats from database....
exports.listAllCats = (req, res) => {
  Cat.find({}, (err, cat) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(200).json(cat);
  });
};

// Creating a new cat and save it to database....
exports.createNewCat = (req, res) => {
  let newCat = new Cat(req.body);
  newCat.save((err, cat) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(201).json(cat);
  });
};

// read a perticular cat by _id......
exports.readCat = (req, body) => {
  Cat.findById(req.params.catid, (err, cat) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(200).json(cat);
  });
};

//Update a perticular cat by _id ....
exports.updateCat = (req, res) => {
  Cat.findOneAndUpdate(
    { _id: req.params.catid },
    req.body,
    { new: true },
    (err, cat) => {
      if (err) {
        res.status(500).send(err);
      }
      res.status(200).json(cat);
    }
  );
};

// Delete a perticular cat by _id .....
exports.deleteCat = (req, res) => {
  Cat.remove({ _id: req.params.catid }, (err, cat) => {
    if (err) {
      res.status(404).send(err);
    }
    res.status(200).json({ message: "Cat successfully deleted" });
  });
};
