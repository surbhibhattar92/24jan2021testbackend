const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const catController = require("./controllers/CatController");

// db instance connection
require("./config/db");

const app = express();

const port = process.env.PORT || 8001;
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(bodyParser.json());

// API ENDPOINTS

app
  .route("/cats")
  .get(catController.listAllCats)
  .post(catController.createNewCat);

app
  .route("/cats/:catid")
  .get(catController.readCat)
  .put(catController.updateCat)
  .delete(catController.deleteCat);

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
